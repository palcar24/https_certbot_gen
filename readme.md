## Instalacion https Digital Ocean

### 1 Instalar certbot en ubuntu20.04 lts


apt update
apt install certbot python3-certbot-nginx -y

### 2 Generar el certificado
service nginx stop
certbot certonly --standalone -d andi2.morant.com.mx
service nginx start

### 3 Automatizar renovacion
echo "2 2 8/5 * * root service nginx stop ;certbot renew ;service nginx start" >> /etc/crontab

### 4 Abrir puerto local
ufw allow 443

### 5 Configurar nginx
cp andi2.morant.com.mx /etc/nginx/sites-available/.

cd /etc/nginx/sites-enable/
ln -s ../sites-available/andi2.morant.com.mx

nginx -t

### 6 Reiniciamos nginx
service nginx start

### 7 Probamos el servicio 
[Acceso al site](https://andi2.morant.com.mx)

